# network-manager-scripts

A collection of Network Manager dispatcher scripts, used to make
system changes when a network connection status changes.


## [apt-changer](apt-changer)

This script allows changing your Apt sources.list configuration files
based on the Wi-Fi network you are connected to, and reverts to a
defined default configuration when a connection is absent.
