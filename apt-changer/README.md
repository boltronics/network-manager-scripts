# apt-changer

This script allows changing your Apt sources.list configuration files
based on the Wi-Fi network you are connected to, and reverts to a
defined default configuration when a connection is absent.

For example, imagine the following setup:
```
$ ls -ld /etc/apt/sources.list*
-rw-r--r-- 1 root root    0 Sep 22  2017  /etc/apt/sources.list
lrwxrwxrwx 1 root root   22 May 17 11:01  /etc/apt/sources.list.d -> sources.list.d.fast_home_wi-fi
drwxr-xr-x 2 root root 4096 Jan 15 11:38  /etc/apt/sources.list.d.common
lrwxrwxrwx 1 root root   19 Oct  4  2017  /etc/apt/sources.list.d.fast_home_wi-fi -> sources.list.d.slow_home_wi-fi
drwxr-xr-x 2 root root 4096 Jan 15 11:46  /etc/apt/sources.list.d.slow_home_wi-fi
lrwxrwxrwx 1 root root   24 Oct  4  2017  /etc/apt/sources.list.d.my_other_workplace -> sources.list.d.my_workplace
drwxr-xr-x 2 root root 4096 Jan 15 11:38  /etc/apt/sources.list.d.ftp.au.debian.org
drwxr-xr-x 2 root root 4096 Jan 15 11:46  /etc/apt/sources.list.d.my_workplace
$ ls -l /etc/apt/sources.list.d.common/
total 12
-rw-r--r-- 1 root root 130 Jan 15 11:13 docker.list
-rw-r--r-- 1 root root 189 Sep 18  2019 google-chrome.list
-rw-r--r-- 1 root root 134 Sep 29  2017 llvm.list
$ ls -l /etc/apt/sources.list.d.my_workplace/
total 12
-rw-r--r-- 1 root root 972 Dec  4 11:26 buster.list
lrwxrwxrwx 1 root root  36 Sep 22  2017 docker.list -> ../sources.list.d.common/docker.list
lrwxrwxrwx 1 root root  43 Jan 15 11:46 google-chrome.list -> ../sources.list.d.common/google-chrome.list
lrwxrwxrwx 1 root root  34 Sep 29  2017 llvm.list -> ../sources.list.d.common/llvm.list
-rw-r--r-- 1 root root 214 Nov  2  2017 sid.list
-rw-r--r-- 1 root root 981 Aug 17  2017 stretch.list
$
```

Here it appears we are connected to an SSID called "fast_home_wi-fi",
as apt-changer has seen this change take effect and updated the
symlink at `/etc/apt/sources.list.d` to point to
`/etc/apt/sources.list.d.fast_home_wi-fi`.

Note that apt-changer only adjusts the `/etc/apt/sources.list.d`
symlink, and does not touch `/etc/apt/sources.list` at all. If you
have entries in there that only apply to specific networks, you will
need to move these entries from this file accordingly.

## Usage

Edit apt-changer as follows:

* Adjust `declare -r wlan_if="wlan0"` to use the Wi-Fi interface name
  you use to fetch package updates.

* Update `declare -r default_config="default"` to match the default
  sources configuration. By default, `/etc/apt/sources.list.d.default`
  will be used if it exists.

Finally, copy the script into `/etc/NetworkManager/dispatcher.d/` and
give it executable permissions. eg. (as root):
```
# cp apt-changer /etc/NetworkManager/dispatcher.d/
# chown root:root /etc/NetworkManager/dispatcher.d/apt-changer
# chmod 0755 /etc/NetworkManager/dispatcher.d/apt-changer
```
